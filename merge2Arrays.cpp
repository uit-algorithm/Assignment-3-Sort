#include <iostream>
#include <vector>

using namespace std;

vector<int> mergeArrays(const vector<int>& arr1, const vector<int>& arr2) {
    vector<int> mergedArr;
    int i = 0, j = 0;

    while (i < arr1.size() && j < arr2.size()) {
        if (arr1[i] < arr2[j]) {
            mergedArr.push_back(arr1[i]);
            i++;
        } else {
            mergedArr.push_back(arr2[j]);
            j++;
        }
    }

    while (i < arr1.size()) {
        mergedArr.push_back(arr1[i]);
        i++;
    }

    while (j < arr2.size()) {
        mergedArr.push_back(arr2[j]);
        j++;
    }

    return mergedArr;
}

int main() {
    int T;
    cin >> T;

    while (T--) {
        int n, m;
        cin >> n >> m;

        vector<int> arr1(n);
        vector<int> arr2(m);

        for (int i = 0; i < n; i++) {
            cin >> arr1[i];
        }

        for (int i = 0; i < m; i++) {
            cin >> arr2[i];
        }

        vector<int> mergedArr = mergeArrays(arr1, arr2);

        for (int i = 0; i < mergedArr.size(); i++) {
            cout << mergedArr[i] << " ";
        }
        cout << endl;
    }

    return 0;
}
