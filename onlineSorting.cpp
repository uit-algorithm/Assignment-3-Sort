#include <iostream>
#include <vector>

using namespace std;
int binarySearch(const std::vector<int>& arr, int target) {
    int left = 0;
    int right = arr.size() - 1;

    while (left <= right) {
        int mid = left + (right - left) / 2;

        if (arr[mid] == target) {
            return mid + 1;
        } else if (arr[mid] < target) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    return 0;
}

void insertAndSort(vector<int>& arr, int newValue) {
    if (binarySearch(arr, newValue) != 0);
    else {
        int n = arr.size();
        int i, key, j;
        arr.push_back(newValue);
        for (i = 1; i < n; i++) {
            key = arr[i];
            j = i - 1;

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }
}

int main() {
    int a, b;
    vector<int> onlineUsers;

    while (true) {
        cin >> a;
        if (a == 0) break;
        cin >> b;

        if (a == 1) insertAndSort(onlineUsers, b);
        else if (a == 2) cout << binarySearch(onlineUsers, b) << endl;
    }
    return 0;
}