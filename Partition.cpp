#include <iostream>
#include <vector>

using namespace std;

int partition(vector<int>& arr, int pivot) {
    int p = -1;

    for (int i = 0; i < arr.size(); i++) {
        if (arr[i] <= pivot) {
            p++;
            swap(arr[i], arr[p]);
        }
    }

    return p;
}

int main() {
    int n, pivot;
    cin >> n >> pivot;

    vector<int> arr(n);

    for (int i = 0; i < n; i++) {
        cin >> arr[i];
    }

    int p = partition(arr, pivot);

    for (int i = 0; i < n; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;

    cout << p << endl;

    return 0;
}
